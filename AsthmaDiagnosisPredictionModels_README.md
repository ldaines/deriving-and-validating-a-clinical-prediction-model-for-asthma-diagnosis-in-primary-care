#The files uploaded to this project are lists of Read code (version 2) used to construct variables from routinely collected (anonymised) medical records.


>  'AsthmaSpecific_ReadcodeList.txt'

#Codelist created by Nissen, F and Quint, JK (2017) Clinical Code List - Specific Asthma Codes. [Data Collection]. London School of Hygiene & #Tropical Medicine, London, United Kingdom. https://doi.org/10.17037/DATA.210. The modified list retains the same codes but is presented as Read codes (version 2) rather than medcodes as in the original.


>  LungFunctionAndReversibility_ReadCodeList.txt'

#Building on 'reversibility' codes from Nissen, F and Quint, JK (2017) this codelist includes  additional Read codes (version 2) used to record lung function testing or reversibility testing in primary care. 

#LDaines 10 March 2020